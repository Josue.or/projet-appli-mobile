package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import fr.uavignon.ceri.tp3.data.Artifact;
import fr.uavignon.ceri.tp3.data.MuseumRepository;

public class ListViewModel extends AndroidViewModel {
    private MuseumRepository repository;
    private MutableLiveData<ArrayList<Artifact>> allArtifacts;

    public ListViewModel (Application application) {
        super(application);
        ArrayList<Artifact> artifacts = new ArrayList<Artifact>();
        allArtifacts = new MutableLiveData<>();
        repository = MuseumRepository.get(application);
        allArtifacts = repository.getArtifactList();
    }

    LiveData<ArrayList<Artifact>> getAllArtifacts() {
        return allArtifacts;
    }

    public void deleteArtifact(String id) {
        repository.deleteArtifact(id);
    }

    public void loadCollection()
    {
        Thread t = new Thread(){
            public void run(){
                repository.loadCollection();
            }
        };
        t.start();
    }

    public void loadCollectionFromDatabase()
    {
        Thread t = new Thread()
        {
            public void run()
            {
                repository.loadCollectionFromDatabase();
            }
        };
        t.start();
    }

    public void loadCollectionFromDatabaseOrderByYear()
    {
        Thread t = new Thread()
        {
            public void run()
            {
                repository.loadCollectionFromDatabaseOrderByYear();
            }
        };
        t.start();
    }


    public void removeAll()
    {
        Thread t = new Thread()
        {
            public void run()
            {
                repository.removeAll();
            }
        };
        t.start();
    }


}
