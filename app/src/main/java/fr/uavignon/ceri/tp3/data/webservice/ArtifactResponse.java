package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;
import java.util.Map;

public class ArtifactResponse {

    public final String name = null;

    public final String description = null;

    public final String brand = null;

    public final Boolean working = false;

    public final Integer year = 0;

    public final Map<String, String> pictures = null;
}
