package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp3.data.Artifact;
import fr.uavignon.ceri.tp3.data.MuseumRepository;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private MuseumRepository repository;
    private MutableLiveData<Artifact> artifact;

    public DetailViewModel(Application application) {
        super(application);
        repository = MuseumRepository.get(application);
        artifact = new MutableLiveData<>();
    }

    public void setArtifact(String id) {
        repository.getArtifact(id);
        artifact = repository.getSelectedArtifact();
    }

    LiveData<Artifact> getArtifact() {
        return artifact;
    }

}

