package fr.uavignon.ceri.tp3.data.webservice;

import java.util.Map;

import fr.uavignon.ceri.tp3.data.Artifact;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface MuseumInterface {
    @Headers( {
            "Accept: json",
    })
    @GET("collection")
    public Call<Map<String, ArtifactResponse>> getCollection();

}
