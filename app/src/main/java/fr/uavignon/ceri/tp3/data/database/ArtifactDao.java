package fr.uavignon.ceri.tp3.data.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Artifact;

@Dao
public interface ArtifactDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Artifact artifact);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Artifact artifact);

    @Query("DELETE FROM Artifact")
    void deleteAll();

    @Query("Delete FROM Artifact where _id = :id")
    void deleteArtifact(String id);

    @Query("Select * from Artifact order by name ASC")
    List<Artifact> getSynchAllArtifacts();

    @Query("Select * from Artifact order by year DESC")
    List<Artifact> getAllArtifactsOrderByYear();

    @Query("Select * from Artifact where _id =:id")
    Artifact getArtifactById(String id);

}
