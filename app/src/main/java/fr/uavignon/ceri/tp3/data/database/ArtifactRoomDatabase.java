package fr.uavignon.ceri.tp3.data.database;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.uavignon.ceri.tp3.data.Artifact;

@Database(entities = {Artifact.class}, version = 16, exportSchema = false)
public abstract class ArtifactRoomDatabase extends RoomDatabase {

    private static final String TAG = ArtifactRoomDatabase.class.getSimpleName();

    public abstract ArtifactDao artifactDao();

    private static ArtifactRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    public static ArtifactRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (ArtifactRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                            // without populate

                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    ArtifactRoomDatabase.class,"artifact_database").fallbackToDestructiveMigration()
                                    .build();

                    //with populate
                       /* INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    ArtifactRoomDatabase.class, "artifact_database")
                                    .addCallback(sRoomDatabaseCallback)
                                    .build();*/


                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);

                    databaseWriteExecutor.execute(() -> {
                        // Populate the database in the background.
                        ArtifactDao dao = INSTANCE.artifactDao();

                    });

                }
            };



}
