package fr.uavignon.ceri.tp3;


import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

import fr.uavignon.ceri.tp3.data.Artifact;


public class RecyclerAdapter extends RecyclerView.Adapter<fr.uavignon.ceri.tp3.RecyclerAdapter.ViewHolder> {

    private static final String TAG = fr.uavignon.ceri.tp3.RecyclerAdapter.class.getSimpleName();

    private ArrayList<Artifact> artifactList;
    private ListViewModel listViewModel;

    private static int CPT = 2;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = null;
        if(CPT % 2 == 0)
        {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout, viewGroup, false);
        }
        else
        {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout_2, viewGroup, false);
        }

        CPT++;

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        viewHolder.itemTitle.setText(artifactList.get(i).getName());

        if(artifactList.get(i).getBrand() == null) viewHolder.itemDetail.setText("Inconnue");
        else
        {
            viewHolder.itemDetail.setText(artifactList.get(i).getBrand());
        }

        if (artifactList.get(i).getPicture() != null)
        {
            Glide.with(viewHolder.itemView.getContext())
                    .load(artifactList.get(i).getPicture())
                    .into(viewHolder.img);
        }
    }

    private void deleteArtifact(String id)
    {
        if (listViewModel != null) listViewModel.deleteArtifact(id);
    }

    @Override
    public int getItemCount() {

        return artifactList == null ? 0 : artifactList.size();
    }

    public void setArtifactList(ArrayList<Artifact> artifacts) {
        artifactList = artifacts;
        notifyDataSetChanged();
    }
    public void setListViewModel(ListViewModel viewModel) {
        listViewModel = viewModel;
    }

     class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemTitle;
        TextView itemDetail;
        ImageView img;

         ActionMode actionMode;
        String idSelectedLongClick;

        ViewHolder(View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemDetail = itemView.findViewById(R.id.item_detail);
            img = itemView.findViewById(R.id.item_image);

            ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

                // Called when the action mode is created; startActionMode() was called
                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    // Inflate a menu resource providing context menu items
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.context_menu, menu);
                    return true;
                }

                // Called each time the action mode is shown. Always called after onCreateActionMode, but
                // may be called multiple times if the mode is invalidated.
                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false; // Return false if nothing is done
                }

                // Called when the user selects a contextual menu item
                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_delete:
                            fr.uavignon.ceri.tp3.RecyclerAdapter.this.deleteArtifact(idSelectedLongClick);
                            Snackbar.make(itemView, "Artefact supprimé !",
                                    Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            mode.finish(); // Action picked, so close the CAB
                            return true;

                        default:
                            return false;
                    }
                }

                // Called when the user exits the action mode
                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    actionMode = null;
                }
            };

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    Log.d(TAG,"position="+getAdapterPosition());
                    String id = RecyclerAdapter.this.artifactList.get((int)getAdapterPosition()).getId();
                    Log.d(TAG,"id="+id);
                    ListFragmentDirections.ActionListFragmentToDetailFragment action = ListFragmentDirections.actionListFragmentToDetailFragment(id);
                    action.setItemNum(id);
                    Navigation.findNavController(v).navigate(action);

                }
            });


            itemView.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    idSelectedLongClick = RecyclerAdapter.this.artifactList.get((int)getAdapterPosition()).getId();
                    if (actionMode != null) {
                        return false;
                    }
                    Context context = v.getContext();
                    // Start the CAB using the ActionMode.Callback defined above
                    actionMode = ((Activity)context).startActionMode(actionModeCallback);
                    v.setSelected(true);
                    return true;
                }
            });
        }




     }

}