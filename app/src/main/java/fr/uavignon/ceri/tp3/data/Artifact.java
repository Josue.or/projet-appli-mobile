package fr.uavignon.ceri.tp3.data;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Entity(tableName = "Artifact", indices = {@Index(value = {"name", "description"},
        unique = true)})
public class Artifact  {

   @PrimaryKey
   @NonNull
   @ColumnInfo(name="_id")
   private String id;

   @NonNull
   @ColumnInfo(name="name")
   private String name;

   @ColumnInfo(name="description")
   private String description = null;

   @ColumnInfo(name="brand")
   private String brand = null;

   @NonNull
   @ColumnInfo(name="working")
   private boolean working = false;

   @NonNull
   @ColumnInfo(name="year")
   private int year = 0;


   @ColumnInfo(name="picture")
   private String picture = null;

   public Artifact(String id, String name, String description)
   {
       this.id = id;
       this.name = name;
       this.description = description;
   }

   public Artifact()
   {}

    @NonNull
   public String getId()
   {
       return id;
   }

   public void setId(String id)
   {
       this.id = id;
   }

    @NonNull
   public String getName()
   {
       return name;
   }

   public void setName(String name)
   {
       this.name = name;
   }

   public String getDescription()
   {
       return description;
   }

   public void setDescription(String description)
   {
       this.description = description;
   }

   public String getBrand()
   {
       return brand;
   }

   public void setBrand(String brand)
   {
       this.brand = brand;
   }

   public boolean getWorking()
   {
       return working;
   }

   public void setWorking(boolean working)
   {
       this.working = working;
   }

   public int getYear()
   {
       return year;
   }

    public void setYear(int year)
    {
        this.year = year;
    }

    public void setPicture(String picture)
    {
        this.picture = picture;
    }

    public String getPicture()
    {
        return picture;
    }
}
