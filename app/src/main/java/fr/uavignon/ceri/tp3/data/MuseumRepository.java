package fr.uavignon.ceri.tp3.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp3.data.database.ArtifactDao;
import fr.uavignon.ceri.tp3.data.database.ArtifactRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.ArtifactResponse;
import fr.uavignon.ceri.tp3.data.webservice.ArtifactResult;
import fr.uavignon.ceri.tp3.data.webservice.MuseumInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.ArtifactRoomDatabase.databaseWriteExecutor;


public class MuseumRepository {

    private static final String TAG = MuseumRepository.class.getSimpleName();

    private MutableLiveData<Artifact> selectedArtifact = new MutableLiveData<>();

    private ArtifactDao artifactDao;

    private final MuseumInterface api;

    private MutableLiveData<ArrayList<Artifact>> artifactList = new MutableLiveData<>();

    private static volatile MuseumRepository INSTANCE;

    public synchronized static MuseumRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new MuseumRepository(application);
        }

        return INSTANCE;
    }

    public MuseumRepository(Application application) {

        ArtifactRoomDatabase db = ArtifactRoomDatabase.getDatabase(application);
        artifactDao = db.artifactDao();

        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(MuseumInterface.class);
    }

    public void getArtifact(String id)
    {
        Future<Artifact> artifact = databaseWriteExecutor.submit(() -> {
           return artifactDao.getArtifactById(id);
        });
        try {
            selectedArtifact.setValue(artifact.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void loadCollectionFromDatabase()
    {
        ArrayList<Artifact> allArtifacts = (ArrayList<Artifact>) artifactDao.getSynchAllArtifacts();
        artifactList.postValue(allArtifacts);
    }

    public void loadCollectionFromDatabaseOrderByYear()
    {
        ArrayList<Artifact> allArtifacts = (ArrayList<Artifact>) artifactDao.getAllArtifactsOrderByYear();
        artifactList.postValue(allArtifacts);
    }

    public void loadCollection() {

        api.getCollection().enqueue(
                new Callback<Map<String, ArtifactResponse>>() {
                    @Override
                    public void onResponse(Call<Map<String, ArtifactResponse>> call,
                                           Response<Map<String, ArtifactResponse>> response) {

                        Log.d("onResponse", response.body().toString());
                        ArrayList<Artifact> artifactList = new ArrayList<>();
                        int i = 0;

                        for(String id: response.body().keySet())
                        {
                            ArtifactResult.transferInfo(response.body().get(id), artifactList, id);

                            insertArtifact(artifactList.get(i));
                            i++;
                        }

                        MuseumRepository.this.artifactList.setValue(artifactList);
                    }

                    @Override
                    public void onFailure(Call<Map<String, ArtifactResponse>> call, Throwable t) {
                        Log.d("onFailure", t.getMessage());
                    }
                });
    }

    public MutableLiveData<Artifact> getSelectedArtifact() {
        return selectedArtifact;
    }




    public long insertArtifact(Artifact newArtifact) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return artifactDao.insert(newArtifact);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedArtifact.setValue(newArtifact);
        return res;
    }


    public void deleteArtifact(String id) {
        databaseWriteExecutor.execute(() -> {
            artifactDao.deleteArtifact(id);
        });
    }



    public MutableLiveData<ArrayList<Artifact>> getArtifactList() {
        return artifactList;
    }

    public void removeAll()
    {
        artifactDao.deleteAll();
        artifactList.postValue(null);
    }
}
