package fr.uavignon.ceri.tp3;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView textName, textDescription, textYear, textBrand, textWorking;
    private ImageView img;
    private ProgressBar progress;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        // Get selected artifact
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        String artifactId = args.getItemNum();
        Log.d(TAG,"selected id="+artifactId);
        viewModel.setArtifact(artifactId);

        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {
        textName = getView().findViewById(R.id.name);
        textBrand = getView().findViewById(R.id.brand);
        textDescription = getView().findViewById(R.id.description);
        textYear = getView().findViewById(R.id.year);
        textWorking = getView().findViewById(R.id.working);
        img = getView().findViewById(R.id.image);




        progress = getView().findViewById(R.id.progress);

        getView().findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp3.DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }

    private void observerSetup() {
        viewModel.getArtifact().observe(getViewLifecycleOwner(),
                artifact -> {

                    if (artifact != null) {

                        textName.setText(artifact.getName());
                        if(artifact.getDescription() != null) textDescription.setText(artifact.getDescription());
                        else textDescription.setText("Pas d'infos supplémentaires");

                        if(artifact.getBrand() == null) textBrand.setText("Inconnue");
                        else textBrand.setText(artifact.getBrand());


                        if(artifact.getWorking()) textWorking.setText("Oui");
                        else textWorking.setText("Non");

                        if(artifact.getYear() == 0 ) textYear.setText("Inconnue");

                        else  textYear.setText(String.valueOf(artifact.getYear()));

                        if(artifact.getPicture() != null)
                        {
                            Glide.with(this)
                                    .load(artifact.getPicture())
                                    .into(img);
                        }
                    }
                });




    }


}