package fr.uavignon.ceri.tp3.data.webservice;

import android.util.Log;

import java.util.ArrayList;

import fr.uavignon.ceri.tp3.data.Artifact;
import fr.uavignon.ceri.tp3.data.webservice.ArtifactResponse;

public class ArtifactResult {

    public static void transferInfo(ArtifactResponse artifactInfo, ArrayList<Artifact> artifacts, String id)
    {
        Artifact artifact = new Artifact();

        artifact.setName(artifactInfo.name);
        artifact.setDescription(artifactInfo.description);
        artifact.setWorking(artifactInfo.working);

        artifact.setId(id);

        artifact.setBrand(artifactInfo.brand);

        artifact.setYear(artifactInfo.year);

        artifact.setPicture("https://demo-lia.univ-avignon.fr/cerimuseum/items/" + id + "/thumbnail"/*+ "/images/" + img*/);


        artifacts.add(artifact);

    }



}
